_default:
    @just --list

_check-sed:
    #!/usr/bin/env bash
    set -euo pipefail

    if [[ ! $(sed --version 2>&1) =~ "GNU sed" ]]; then
      echo "GNU sed is required!"
      exit 1
    fi

# Build Docker image
build:
    ./build.sh

# Run pre-commit hooks
lint:
    pre-commit run --all-files

# Test running GitLab CI locally
test-gitlab-ci:
    sudo gitlab-runner exec docker \
      --docker-privileged \
      --docker-volumes "/certs/client" \
      --env "GITLAB_USER=$GITLAB_USER" \
      --env "GITLAB_TOKEN=$GITLAB_TOKEN" \
      docker_build

# Update nixpkgs rev in default.nix, Dockerfile
update: _check-sed
    #!/usr/bin/env bash
    set -euo pipefail

    nix flake update
    cd ../ansible-openwrt-imagebuilder && nix flake update && cd -
    digest_latest=$(xh \
        https://hub.docker.com/v2/repositories/nixos/nix/tags/latest | \
        jq -r '.images[0].digest')
    sed -i "/FROM nixos\/nix/c FROM nixos\/nix@$digest_latest" Dockerfile
