#!/usr/bin/env bash

DOCKER_TAG_VERSION=$(date +"%Y-%m-%d")
DOCKER_TAG="registry.gitlab.com/austinbutler/docker-openwrt-build/dind:$DOCKER_TAG_VERSION"

sudo docker build -t "$DOCKER_TAG" .
echo "$GITLAB_TOKEN" | sudo docker login -u "$GITLAB_USER" --password-stdin registry.gitlab.com
sudo docker push "$DOCKER_TAG"
