#!/usr/bin/env bash

DOCKER_TAG_VERSION=$(date +"%Y-%m-%d")
DOCKER_TAG="registry.gitlab.com/austinbutler/docker-openwrt-build:${DOCKER_TAG_VERSION}"

docker build -t "$DOCKER_TAG" .
