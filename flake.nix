{
  description = "Docker image based on Nix, with Ansible and the needed packages to build OpenWRT";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat.url = "github:edolstra/flake-compat";
    flake-compat.flake = false;
    git-hooks.url = "github:cachix/git-hooks.nix";
  };

  outputs =
    {
      nixpkgs,
      flake-utils,
      git-hooks,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = { };
          overlays = [
            (
              final: prev:
              {
              }
            )
          ];
        };

        pre-commit-check = git-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            check-case-conflict = {
              enable = true;
              name = "check for case conflicts";
              description = "checks for files that would conflict in case-insensitive filesystems.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-case-conflict";
            };
            check-executables-have-shebangs = {
              enable = true;
              name = "check that executables have shebangs";
              description = "ensures that (non-binary) executables have a shebang.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-executables-have-shebangs";
              types = [
                "text"
                "executable"
              ];
            };
            check-json = {
              enable = true;
              name = "check json";
              description = "checks json files for parseable syntax.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-json";
              types = [ "json" ];
            };
            check-merge-conflict = {
              enable = true;
              name = "check for merge conflicts";
              description = "checks for files that contain merge conflict strings.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-merge-conflict";
              types = [ "text" ];
            };
            check-shebang-scripts-are-executable = {
              enable = true;
              name = "check that scripts with shebangs are executable";
              description = "ensures that (non-binary) files with a shebang are executable.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-shebang-scripts-are-executable";
              types = [ "text" ];
            };
            check-vcs-permalinks = {
              enable = true;
              name = "check vcs permalinks";
              description = "ensures that links to vcs websites are permalinks.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-vcs-permalinks";
              types = [ "text" ];
            };
            check-yaml = {
              enable = true;
              name = "check yaml";
              description = "checks yaml files for parseable syntax.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/check-yaml";
              types = [ "yaml" ];
            };
            detect-private-key = {
              enable = true;
              name = "detect private key";
              description = "detects the presence of private keys.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/detect-private-key";
              types = [ "text" ];
            };
            end-of-file-fixer = {
              enable = true;
              name = "fix end of files";
              description = "ensures that a file is either empty, or ends with one newline.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/end-of-file-fixer";
              types = [ "text" ];
            };
            hadolint.enable = true;
            mixed-line-ending = {
              enable = true;
              name = "mixed line ending";
              description = "replaces or checks mixed line ending.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/mixed-line-ending --fix=lf";
              types = [ "text" ];
            };
            nixfmt-rfc-style.enable = true;
            nil.enable = true;
            prettier = {
              enable = true;
              types = [
                "json"
                "markdown"
                "yaml"
              ];
            };
            shellcheck = {
              enable = true;
              types_or = nixpkgs.lib.mkForce [ "shell" ];
            };
            shfmt.enable = true;
            statix.enable = true;
            trailing-whitespace = {
              enable = true;
              name = "trim trailing whitespace";
              description = "trims trailing whitespace.";
              entry = "${pkgs.python3Packages.pre-commit-hooks}/bin/trailing-whitespace-fixer";
              types = [ "text" ];
            };
            # TODO: Enable meta hooks
            # check-hooks-apply = {
            #   enable = true;
            #   name = "check hooks";
            #   entry = "${preCommitEnv}/bin/python -m pre_commit.meta_hooks.check-hooks-apply";
            # };
          };
        };

        pythonEnv = pkgs.python3.withPackages (ps: [
          ps.distutils
        ]);
      in
      {
        devShells.default = pkgs.mkShell {
          name = "docker-openwrt-build";

          buildInputs = with pkgs; [
            git
            gitlab-runner
            gnused
            hadolint
            just
            nixfmt-rfc-style
            nodePackages.prettier
            pre-commit
            pythonEnv
            shellcheck
            shfmt
          ];

          shellHook = ''
              ${pre-commit-check.shellHook}

            # Set up VS Code config files if they don't exist
            if [[ ! -f ./.vscode/settings.json && -f ./vscode/settings.sample.json ]]; then cp ./.vscode/settings.sample.json ./.vscode/settings.json; fi
            if [[ ! -f ./.vscode/launch.json && -f ./vscode/launch.sample.json ]]; then cp ./.vscode/launch.sample.json ./.vscode/launch.json; fi
          '';
        };

        devShells.openwrt = pkgs.mkShell {
          name = "openwrt";

          buildInputs = with pkgs; [
            ansible
            bashInteractive
            bzip2
            diffutils
            file
            gawk
            git
            gnugrep
            gnumake
            gnutar
            perl
            pythonEnv
            rsync
            unixtools.getopt
            unzip
            wget
            which
            xz
            zstd
          ];
        };
      }
    );
}
