FROM docker.io/nixos/nix@sha256:a0802fedf3509ee4a0bf0bc4533bf3e216460b92edc0bbb6e86f9d520a7b100a

RUN mkdir -p ~/.config/nix && echo 'extra-experimental-features = flakes nix-command' > ~/.config/nix/nix.conf

WORKDIR /opt/imagebuilder

COPY flake.nix .
COPY flake.lock .

RUN nix develop '.#openwrt'
